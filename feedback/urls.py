from django.conf.urls.defaults import *


from views import *


urlpatterns = patterns('',
    url(r'^$', ListAnswers.as_view(), name='list_answers'),
    url(r'^form/$', SendMessage.as_view(), name='send_message'),
)
