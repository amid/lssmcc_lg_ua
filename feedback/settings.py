from django.conf import settings


def get(key, default):
    return getattr(settings, key, default)


NOTIFICATION_MAIL = get('FEEDBACK_NOTIFICATION_MAIL', 'mail@lssmcc.lg.ua')
NOREPLY_MAIL = get('FEEDBACK_NOREPLY_MAIL', 'noreply@lssmcc.lg.ua')
