# -- coding: utf-8 --

from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.mail.message import EmailMessage
from django.db import models


import feedback.settings as feedback_settings


class Message(models.Model):
    
    name = models.CharField(max_length=50, verbose_name=u'Ваше і\'мя')
    email = models.EmailField(null=True, blank=True, verbose_name=u'Електронна скринька', help_text=u'Якщо вказано адресу ел. поштової скриньки, Ви отримаєти відповідь також на цю ел. скриньку')
    title = models.CharField(max_length=150, verbose_name=u'Тема запитання', null=True, blank=True)
    message = models.TextField(verbose_name=u'Зміст запитання')
    timestamp = models.DateTimeField(auto_now=True)
    mark_as_read = models.BooleanField(default=False)
    site = models.ForeignKey(Site)
    
    class Meta:
        
        ordering = ['-timestamp']
        verbose_name = u'повідомлення'
        verbose_name_plural = u'повідомлення'
    
    def __unicode__(self):
        if self.title:
            title = self.title
        else:
            title = u'Немає теми'
        return '%s - %s<%s>: %s' % (
            self.timestamp.strftime('%d.%m.%Y'),
            self.name,
            self.email,
            title
        )


class Answer(models.Model):
    
    question = models.ForeignKey('Message')
    question_is_valid = models.BooleanField(default=True)
    answer = models.TextField(verbose_name=u'відповідь')
    answer_is_valid = models.BooleanField(default=False)
    send_email = models.BooleanField(default=False)
    
    class Meta:
        
        ordering = ['question']
        verbose_name = u'відповідь'
        verbose_name_plural = u'відповіді на запитання'
    
    def __unicode__(self):
        return '%s %s' % (u'Відповідь на', self.answer)
    
    def save(self, **kwargs):
        super(Answer, self).save(**kwargs)
        if self.answer_is_valid:
            question = self.question
            question.mark_as_read = True
            question.save()
            if question.email is not None and self.send_email:
                mail = EmailMessage(
                    u'Відповідь з сайту %s' % Site.objects.get_current(),
                    render_to_string(
                        'feedback/answer_email.txt',
                        {
                            'site': Site.objects.get_current(),
                            'name': question.name,
                            'subject': question.title,
                            'message': question.message,
                            'answer': obj.answer,
                            'url': self.get_absolute_url()
                        }
                    ),
                    feedback_settings.NOREPLY_MAIL,
                    [question.email],
                )
                mail.content_subtype = 'text'
                mail.send(fail_silently=True)
        
