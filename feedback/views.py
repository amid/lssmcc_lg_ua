# -- coding: utf-8 --

from django.conf import settings
from django.contrib import messages
from django.contrib.sites.models import Site
from django.core.mail.message import EmailMessage
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.views.generic import CreateView, ListView


from models import *
from forms import *
import feedback.settings as feedback_settings


from recaptcha.client import captcha


class SendMessage(CreateView):
    
    model = Message
    form_class = SendMessageForm
    
    def get_success_url(self):
        return reverse('feedback:list_answers')
    
    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        check_captcha = captcha.submit(
            request.POST['recaptcha_challenge_field'],
            request.POST['recaptcha_response_field'],
            settings.RECAPTCHA_PRIVATE_KEY,
            request.META['REMOTE_ADDR']
        )
        if form.is_valid() and check_captcha.is_valid:
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
    
    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.site = Site.objects.get_current()
        obj.save()
        self.object = obj
        messages.success(self.request, u'Ваше запитання запитання відправлено. Дякуємо.')
        try:
            mail = EmailMessage(
                u'Лист з сайту',
                render_to_string(
                    'feedback/mail/you_have_new_message.txt',
                    {
                        'site': Site.objects.get_current(),
                        'name': obj.name,
                        'email': obj.email,
                        'subject': obj.title,
                        'message': obj.message,
                    }
                ),
                feedback_settings.NOREPLY_MAIL,
                [feedback_settings.NOTIFICATION_MAIL],
            )
            mail.content_subtype = 'text'
            mail.send(fail_silently=True)
        except:
            pass
        return super(SendMessage, self).form_valid(form)
    
    def get_context_data(self, **kwargs):
        context = super(SendMessage, self).get_context_data(**kwargs)
        context['form_legend'] = u'Задати запитання'
        context['html_captcha'] = captcha.displayhtml(settings.RECAPTCHA_PUB_KEY)
        context['current_menu_section'] = 'feedback'
        return context


class ListAnswers(ListView):
    
    model = Answer
    queryset = Answer.objects.filter(question_is_valid=True, answer_is_valid=True)
    
    def get_context_data(self, **kwargs):
        context = super(ListAnswers, self).get_context_data(**kwargs)
        context['current_menu_section'] = 'feedback'
        return context
