from django import forms
from django.contrib.auth.models import User


from models import *
from fields import ReCaptchaField


class SendMessageForm(forms.ModelForm):
    
    class Meta:
        
        model = Message
        exclude = ('timestamp', 'mark_as_read', 'site')
    
    def __init__(self, *args, **kwargs):
            
        super(SendMessageForm, self).__init__(*args, **kwargs)
        
        self.fields['name'].widget.attrs['class'] = 'input-xlarge'
        self.fields['email'].widget.attrs['class'] = 'input-xlarge'
        self.fields['title'].widget.attrs['class'] = 'input-xxlarge'
        self.fields['message'].widget.attrs['class'] = 'input-xxlarge'
