# -- coding: utf-8 --

from django import forms
from django.contrib import admin
from django.contrib.sites.models import Site


from models import *


def mark_items_as_read(modeladmin, request, queryset):
    queryset.update(mark_as_read=True)
mark_items_as_read.short_description = u'Відмітити, як орброблені'


def mark_items_as_unread(modeladmin, request, queryset):
    queryset.update(mark_as_read=False)
mark_items_as_unread.short_description = u'Відмітити, як НЕ орброблені'


class MessageAdminForm(forms.ModelForm):
    
    class Meta:
        model = Message
        exclude = ('site',)


class MessageAdmin(admin.ModelAdmin):
    
    list_display = ('display_name', 'display_timestamp', 'title', 'mark_as_read')
    list_filter = ('mark_as_read',)
    date_hierarchy = 'timestamp'
    ordering = ('-timestamp',)
    actions = [mark_items_as_read, mark_items_as_unread]
    form = MessageAdminForm
    
    def has_add_permission(self, request):
        return False
    
    def get_site(self):
        return Site.objects.get_current()
    
    def display_name(self, obj):
        return '%s <%s>' % (obj.name, obj.email)
    display_name.short_description = u'І\'мя'
    
    def display_timestamp(self, obj):
        return obj.timestamp.strftime('%d.%m.%Y')
    display_timestamp.short_description = u'Дата'
    
    def queryset(self, request):
        return self.model.objects.filter(site=self.get_site())


class AnswerAdmin(admin.ModelAdmin):
    
    list_filter = ('question_is_valid',)


admin.site.register(Message, MessageAdmin)
admin.site.register(Answer, AnswerAdmin)
