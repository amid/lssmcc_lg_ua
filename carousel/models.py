from django.conf import settings
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver


import os, shutil


def photo_upload_dir(id):
    return os.path.join(settings.MEDIA_ROOT, 'carousel', str(id))


def make_photo_upload_path(instance, filename):
    return os.path.join(photo_upload_dir(instance.pk), filename)


class Carousel(models.Model):
    title = models.CharField(max_length=60)
    url = models.CharField(max_length=255)
    body = models.TextField(blank=True, null=True)
    index = models.PositiveIntegerField(default=1000)
    photo = models.ImageField(upload_to=make_photo_upload_path)
    
    class Meta:
        ordering = ['index']
    
    def __unicode__(self):
        return self.title


@receiver(post_delete, sender=Carousel)
def del_uploads(sender, **kwargs):
    try:
        shutil.rmtree(photo_upload_dir(kwargs['instance'].id))
    except:
        pass
