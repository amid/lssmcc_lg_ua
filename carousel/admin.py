from django.contrib import admin


from models import *


class CarouselAdmin(admin.ModelAdmin):
    pass


admin.site.register(Carousel, CarouselAdmin)
