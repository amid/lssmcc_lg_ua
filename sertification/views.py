# -*- coding: utf-8 -*-

from django.conf import settings
from django.views.generic import TemplateView

class SertificationView(TemplateView):
    
    template_name = 'sertification.html'
    
    def get_context_data(self, **kwargs):
        f = open(settings.SERTIFICATION_FILE, 'r')
        data = f.read()
        f.close()
        print data
        return {
            'data': data,
            'current_menu_section': 'certyfkovani-pidpryemstva'
        }
