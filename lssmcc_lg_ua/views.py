from django.views.generic import TemplateView


from carousel.models import Carousel


class HomeView(TemplateView):

    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['carousel'] = Carousel.objects.all()
        context['current_menu_section'] = 'na-golovny'
        return context
