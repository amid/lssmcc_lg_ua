from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin


admin.autodiscover()


from views import *


urlpatterns = patterns('',
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^entries/', include('entries.urls', namespace='entries')),
    url(r'^feedback/', include('feedback.urls', namespace='feedback')),
    url(r'^sertification/', include('sertification.urls', namespace='sertification')),
    url(r'^admin/', include(admin.site.urls)),
)


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.MEDIA_ROOT, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
