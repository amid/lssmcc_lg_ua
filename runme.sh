#!/bin/bash

rm db.sqlite
source ./env/bin/activate
python manage.py syncdb
python manage.py createsuperuser --username=admin --email=q@q.com
python manage.py migrate
python data.py
