from django.contrib import admin


from models import *


class CommonGroupAdmin(admin.ModelAdmin):
    
    list_filter = ('is_active',)


class CommonItemAdmin(admin.ModelAdmin):
    
    list_filter = ('is_active', 'group')


admin.site.register(PlaceholderGroup, CommonGroupAdmin)
admin.site.register(Placeholder, CommonItemAdmin)
admin.site.register(FilterGroup, CommonGroupAdmin)
admin.site.register(Filter, CommonItemAdmin)
