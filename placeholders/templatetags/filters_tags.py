from django import template
from django.template.loader import get_template
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse


from placeholders.models import *


register = template.Library()


def _get_filters(user, **kwargs):
    kwargs.update({
        'only_for_superusers': False,
        'only_for_authenticated_users': False,
    })
    try:
        if user.is_authenticated():
            del kwargs['only_for_authenticated_users']
            if user.is_superuser:
                del kwargs['only_for_superusers']
    except:
        pass
    return Filter.active_objects.filter(**kwargs)


@register.simple_tag(takes_context=True)
def get_filters(context, **kwargs):
    return _get_filters(context['user'], **kwargs)


@register.simple_tag(takes_context=True)
def render_filters(context, **kwargs):
    return get_template("filters/filters_list.html").render(template.Context({
        'filters_list': _get_filters(context['user'], **kwargs),
        'current_menu_section': context['current_menu_section'],
    }))
