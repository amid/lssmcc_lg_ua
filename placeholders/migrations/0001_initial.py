# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PlaceholderGroup'
        db.create_table('placeholders_placeholdergroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('index', self.gf('django.db.models.fields.PositiveIntegerField')(default=1000)),
        ))
        db.send_create_signal('placeholders', ['PlaceholderGroup'])

        # Adding model 'FilterGroup'
        db.create_table('placeholders_filtergroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('index', self.gf('django.db.models.fields.PositiveIntegerField')(default=1000)),
        ))
        db.send_create_signal('placeholders', ['FilterGroup'])

        # Adding model 'Placeholder'
        db.create_table('placeholders_placeholder', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('index', self.gf('django.db.models.fields.PositiveIntegerField')(default=1000)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['placeholders.PlaceholderGroup'], null=True, blank=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('template', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal('placeholders', ['Placeholder'])

        # Adding model 'Filter'
        db.create_table('placeholders_filter', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=64)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('index', self.gf('django.db.models.fields.PositiveIntegerField')(default=1000)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['placeholders.FilterGroup'], null=True, blank=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('element_kwargs', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('element_ordering', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('element_limit', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('element_template', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('only_for_superusers', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('only_for_authenticated_users', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('show_if_empty', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('placeholders', ['Filter'])


    def backwards(self, orm):
        # Deleting model 'PlaceholderGroup'
        db.delete_table('placeholders_placeholdergroup')

        # Deleting model 'FilterGroup'
        db.delete_table('placeholders_filtergroup')

        # Deleting model 'Placeholder'
        db.delete_table('placeholders_placeholder')

        # Deleting model 'Filter'
        db.delete_table('placeholders_filter')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'placeholders.filter': {
            'Meta': {'object_name': 'Filter'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'element_kwargs': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'element_limit': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'element_ordering': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'element_template': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['placeholders.FilterGroup']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1000'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'only_for_authenticated_users': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'only_for_superusers': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'show_if_empty': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        'placeholders.filtergroup': {
            'Meta': {'object_name': 'FilterGroup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1000'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        'placeholders.placeholder': {
            'Meta': {'object_name': 'Placeholder'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['placeholders.PlaceholderGroup']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1000'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'template': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        'placeholders.placeholdergroup': {
            'Meta': {'object_name': 'PlaceholderGroup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1000'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        }
    }

    complete_apps = ['placeholders']