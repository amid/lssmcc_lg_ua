from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext as _


import ast
from pytils.translit import slugify


class ActiveManager(models.Manager):
    def get_query_set(self):
        return super(ActiveManager, self).get_query_set().filter(is_active=True)


class CommonModel(models.Model):
    
    is_active = models.BooleanField(default=True, verbose_name=_('is active'))
    name = models.CharField(max_length=64, unique=True, verbose_name=_('name'))
    title = models.CharField(max_length=64, verbose_name=_('title'))
    index = models.PositiveIntegerField(default=1000, verbose_name=_('index'), help_text=_('for ordering'))
    
    objects = models.Manager()
    active_objects = ActiveManager()
    
    class Meta:
        
        ordering = ['index']
        abstract = True
    
    def save(self, **kwargs):
        if not self.name:
            self.name = slugify(self.title)
        super(CommonModel, self).save(**kwargs)
    
    def __unicode__(self):
        return self.title


class PlaceholderGroup(CommonModel):
    
    class Meta:
        verbose_name = _('placeholder group')
        verbose_name_plural = _('plachelder groups')


class FilterGroup(CommonModel):
    
    class Meta:
        verbose_name = _('filter group')
        verbose_name_plural = _('filter groups')


class Placeholder(CommonModel):
    
    group = models.ForeignKey('PlaceholderGroup', null=True, blank=True, verbose_name=_('group'))
    content_type = models.ForeignKey(ContentType, verbose_name=_('model'))
    template = models.CharField(max_length=255, null=True, blank=True, verbose_name=_('template'))
    
    class Meta:
        verbose_name = _('placeholder')
        verbose_name_plural = _('plachelders')


class Filter(CommonModel):
    
    group = models.ForeignKey('FilterGroup', null=True, blank=True, verbose_name=_('group'))
    content_type = models.ForeignKey(ContentType, verbose_name=_('model'))
    element_kwargs = models.CharField(max_length=255, null=True, blank=True, verbose_name=_('kwargs'), help_text=_('python dictionary with django queryset kwargs'))
    element_ordering = models.CharField(max_length=32, null=True, blank=True, verbose_name=_('oredering'), help_text=_('if not set - using model ordering; you can use several params: param1, param2, -param3'))
    element_limit = models.PositiveIntegerField(null=True, blank=True, verbose_name=_('limit'))
    element_template = models.CharField(max_length=255, null=True, blank=True, verbose_name=_('element template'))
    only_for_superusers = models.BooleanField(default=False, verbose_name=_('show filter for superusers'))
    only_for_authenticated_users = models.BooleanField(default=False, verbose_name=_('show filter for authenticated users'))
    show_if_empty = models.BooleanField(default=False, verbose_name=_('show if filter is empty'))
    
    class Meta:
        verbose_name = _('model filter')
        verbose_name_plural = _('model filters')
    
    def elements(self):
        model_class = ContentType.objects.get(pk=self.content_type.pk).model_class()
        if self.element_ordering:
            ordering = map(lambda x: x.strip(), self.element_ordering.split(','))
        else:
            ordering = model_class._meta.ordering
        result = []
        if self.element_kwargs:
            return model_class._default_manager.filter(**ast.literal_eval(self.element_kwargs)).order_by(*ordering)[:self.element_limit]
        else:
            return model_class._default_manager.all().order_by(*ordering)[:self.element_limit]
