# -- coding: utf-8 --

from django.contrib import admin


class ChaptersMenuFilter(admin.SimpleListFilter):
    
    title = u'меню'
    parameter_name = 'menu_placeholder'

    def lookups(self, request, model_admin):
        return (
            ('main_menu', 'Головне меню'),
            ('sidebar_menu', 'Правий бік'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'main_menu':
            return queryset.filter(placeholders__name='main_menu')
        if self.value() == 'sidebar_menu':
            return queryset.filter(placeholders__group__name='sidebar_menu')


class ChaptersSidebarMenuFilter(admin.SimpleListFilter):
    
    title = u'меню з боку'
    parameter_name = 'sidebar_menu_placeholder'

    def lookups(self, request, model_admin):
        return (
            ('sidebar_navigation_menu', 'Навігація по сайту'),
            ('sidebar_news_and_publications_menu', 'Новини та публікації'),
            ('sidebar_projects_menu', 'Проекти'),
            ('sidebar_information_menu', 'Інформ. про підпр-во'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'sidebar_navigation_menu':
            return queryset.filter(placeholders__name='sidebar_navigation_menu')
        if self.value() == 'sidebar_news_and_publications_menu':
            return queryset.filter(placeholders__name='sidebar_news_and_publications_menu')
        if self.value() == 'sidebar_projects_menu':
            return queryset.filter(placeholders__name='sidebar_projects_menu')
        if self.value() == 'sidebar_information_menu':
            return queryset.filter(placeholders__name='sidebar_information_menu')


class ChaptersShowControlsFilter(admin.SimpleListFilter):
    
    title = u'шаблонами'
    parameter_name = 'show_controls_placeholder'

    def lookups(self, request, model_admin):
        return (
            ('show_controls', 'Повні шаблони'),
            ('hide_controls', 'Скорочені шаблони'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'show_controls':
            return queryset.filter(placeholders__name='show_controls')
        if self.value() == 'hide_controls':
            return queryset.exclude(placeholders__name='show_controls')
