# -- coding: utf-8 --

from django.contrib import admin
from django.core.urlresolvers import reverse


from entries.admin.chapters.filters import ChaptersMenuFilter, ChaptersSidebarMenuFilter, ChaptersShowControlsFilter
from entries.admin.chapters.forms import ChapterAdminForm


def mark_as_active(modeladmin, request, queryset):
    queryset.update(is_active=True)
mark_as_active.short_description = 'Помітити розділи як діючі'


def mark_as_deleted(modeladmin, request, queryset):
    queryset.update(is_active=False)
mark_as_deleted.short_description = 'Помітити розділи як НЕ діючі'


class ChaptersAdmin(admin.ModelAdmin):
    
    actions = [mark_as_active, mark_as_deleted]
    fieldsets = (
        ('Основні параметри', {
            'fields': ('short_title', 'full_title', 'show_title', 'ancestor', )
        }),
        ('Додаткові параметри', {
            'fields': ('slug', 'index', 'url', 'keywords', )
        }),
        ('Placeholders', {
            'fields': ('placeholders', )
        }),
        ('Параметри контенту', {
            'classes': ('collapse', ),
            'fields': ('template', )
        }),
        ('Усілякі хітрожопі брудні хаки по вибірці новин', {
            'classes': ('collapse',),
            'fields': ('additional_news_kwargs', 'or_news_kwargs', 'exclipt_news_kwargs', )
        }),
        ('Відмітка про видалення', {
            'classes': ('collapse',),
            'fields': ('is_active', )
        }),
    )
    form = ChapterAdminForm
    list_display = ('short_title', 'is_active', 'view_link', 'tricky_chapter', 'index', 'ancestor_chapter', 'news_count', 'news_link')
    list_filter = ('is_active', ChaptersMenuFilter, ChaptersSidebarMenuFilter, ChaptersShowControlsFilter, )
    save_on_top = True
    
    def ancestor_chapter(self, obj):
        if obj.ancestor:
            return u'<a href="%s">%s</a>' % (reverse('admin:entries_chapter_change', args=(obj.id,)), obj.ancestor)
        else:
            return ''
    ancestor_chapter.allow_tags = True
    ancestor_chapter.short_description = 'Розділ-предок'
    
    def news_count(self, obj):
        return obj.news_count()
    news_count.short_description = 'Кількість новин'
    
    def news_link(self, obj):
        return u'<a href="%s?chapter__id__exact=%d">новини</a>' % (reverse('admin:entries_newsentry_changelist'), obj.pk)
    news_link.short_description = 'Показати'
    news_link.allow_tags = True
    
    def tricky_chapter(self, obj):
        if obj.additional_news_kwargs is not None and obj.additional_news_kwargs != '':
            return '<img alt="True" src="/static/admin/img/icon-yes.gif">'
        if obj.or_news_kwargs is not None and obj.or_news_kwargs != '':
            return '<img alt="True" src="/static/admin/img/icon-yes.gif">'
        if obj.exclipt_news_kwargs is not None and obj.exclipt_news_kwargs != '':
            return '<img alt="True" src="/static/admin/img/icon-yes.gif">'
        return ''
    tricky_chapter.allow_tags = True
    tricky_chapter.short_description = 'Хітрожопий розділ :)'
    
    def view_link(self, obj):
        return u'<a href="%s">на сайті</a>' % obj.get_absolute_url()
    view_link.short_description = 'Дивитися'
    view_link.allow_tags = True
    

