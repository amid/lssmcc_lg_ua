from django.contrib import admin
from django import forms


from entries.models import Chapter
from placeholders.models import Placeholder


class ChapterAdminForm(forms.ModelForm):
    
    class Meta:
        
        model = Chapter
    
    def __init__(self, *args, **kwargs):
        super(ChapterAdminForm, self).__init__(*args, **kwargs)
        self.fields['placeholders'] = forms.ModelMultipleChoiceField(
            label = 'params',
            queryset = Placeholder.objects.filter(content_type__app_label='entries', content_type__model='chapter'),
            required = False,
            widget = forms.CheckboxSelectMultiple(),
        )
