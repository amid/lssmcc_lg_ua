from django.contrib import admin
from django import forms


from entries.models import NewsEntry
from placeholders.models import Placeholder


class NewsAdminForm(forms.ModelForm):
    
    galeries = forms.CharField(required=False, initial='', widget=forms.Textarea(attrs={'readonly': True, 'class': 'vTextField'}))
    inlines = forms.CharField(required=False, initial='', widget=forms.Textarea(attrs={'readonly': True, 'class': 'vTextField'}))
    
    class Meta:
        
        model = NewsEntry
    
    def __init__(self, *args, **kwargs):
        super(NewsAdminForm, self).__init__(*args, **kwargs)
        self.fields['placeholders'] = forms.ModelMultipleChoiceField(
            label = 'params',
            queryset = Placeholder.objects.filter(content_type__app_label='entries', content_type__model='newsentry'),
            required = False,
            widget = forms.CheckboxSelectMultiple(),
        )
        self.fields['full_body'].widget.attrs['rows'] = 40
        self.fields['short_body'].widget.attrs['rows'] = 20
        self.fields['full_body_html'].widget.attrs['rows'] = 40
        self.fields['short_body_html'].widget.attrs['rows'] = 20
        if getattr(self.instance, 'pk', -1) >= 0:
            for attachment in self.instance.get_media_items()['galeries']:
                self.fields['galeries'].initial += attachment['markup'] + '\n'
            for attachment in self.instance.get_media_items()['inlines']:
                self.fields['inlines'].initial += attachment['markup'] + '\n'
