# -- coding: utf-8 --

from django.conf import settings
from django.contrib import admin
from django.core.urlresolvers import reverse


from entries.admin.news.forms import NewsAdminForm
from entries.models import NewsEntry


import datetime


def mark_as_active(modeladmin, request, queryset):
    queryset.update(is_active=True)
mark_as_active.short_description = 'Помітити новини як діючі'


def mark_as_deleted(modeladmin, request, queryset):
    queryset.update(is_active=False)
mark_as_deleted.short_description = 'Помітити новини як НЕ діючі'


def mark_as_published(modeladmin, request, queryset):
    queryset.update(is_published=True, published=datetime.datetime.now())
mark_as_published.short_description = 'Помітити новини як опубліковані'


def mark_as_unpublished(modeladmin, request, queryset):
    queryset.update(is_published=False, published=None)
mark_as_unpublished.short_description = 'Помітити новини як НЕ опубліковані'


class NewsAdmin(admin.ModelAdmin):
    
    class Media:
        css = {
            "all": ('%sentries/css/admin-override.css' % settings.STATIC_URL,)
        }
    
    actions = [mark_as_published, mark_as_unpublished, mark_as_active, mark_as_deleted]
    #~ change_form_template = 'entries/admin/news_change_form.html'
    date_hierarchy = 'published'
    fieldsets = (
        ('Основні параметри', {
            'fields': ('chapter', 'short_title', 'full_title', 'show_title', )
        }),
        ('Дані про публікацію', {
            'fields': ('is_published', 'published', )
        }),
        ('Додаткові параметри', {
            'fields': ('index', 'tag_list', 'keywords', )
        }),
        ('Placeholders', {
            'fields': ('placeholders', )
        }),
        ('Зміст', {
            'fields': ('full_body', 'galeries', 'inlines')
        }),
        ('Короткий зміст', {
            'classes': ('collapse',),
            'fields': ('short_body', )
        }),
        ('HTML', {
            'classes': ('collapse',),
            'fields': ('use_raw_html', 'full_body_html', 'short_body_html', )
        }),
        ('Відмітка про видалення', {
            'classes': ('collapse',),
            'fields': ('is_active', )
        }),
    )
    form = NewsAdminForm
    list_display = ('short_title', 'is_active', 'is_published', 'views_count', 'chapter_link', 'creation_date', 'publication_date', 'index', 'is_on_homepage', 'is_in_anouncements', 'is_in_news', 'is_in_publications', 'uses_raw_html', )
    #~ list_filter = ('chapter',)
    save_on_top = True
    
    #~ class Media:
        #~ js = ('https://raw.github.com/suprotkin/django-admin-select-filters/master/rsFilterSelect.js',)
    
    def chapter_link(self, obj):
        return u'<a href="%s">%s</a>' % (reverse('admin:entries_chapter_change', args=(obj.chapter.pk,)), obj.chapter)
    chapter_link.allow_tags = True
    chapter_link.short_description = 'Розділ'
    
    def is_on_homepage(self, obj):
        if 'show_on_homepage' in obj.placeholders.all().values_list('name', flat=True):
            return '<img alt="True" src="/static/admin/img/icon-yes.gif">'
        else:
            return '<img alt="True" src="/static/admin/img/icon-no.gif">'
    is_on_homepage.allow_tags = True
    is_on_homepage.short_description = 'На головній'
    
    def is_in_anouncements(self, obj):
        if 'show_in_anouncements' in obj.placeholders.all().values_list('name', flat=True):
            return '<img alt="True" src="/static/admin/img/icon-yes.gif">'
        else:
            return '<img alt="True" src="/static/admin/img/icon-no.gif">'
    is_in_anouncements.allow_tags = True
    is_in_anouncements.short_description = 'У анонсах'
    
    def is_in_news(self, obj):
        if 'show_in_news' in obj.placeholders.all().values_list('name', flat=True) or obj.chapter.slug == 'novini':
            return '<img alt="True" src="/static/admin/img/icon-yes.gif">'
        else:
            return '<img alt="True" src="/static/admin/img/icon-no.gif">'
    is_in_news.allow_tags = True
    is_in_news.short_description = 'У новинах'
    
    def is_in_publications(self, obj):
        if 'show_in_publications' in obj.placeholders.all().values_list('name', flat=True):
            return '<img alt="True" src="/static/admin/img/icon-yes.gif">'
        else:
            return '<img alt="True" src="/static/admin/img/icon-no.gif">'
    is_in_publications.allow_tags = True
    is_in_publications.short_description = 'У публікаціях'
    
    def creation_date(self, obj):
        return obj.created.strftime('%d.%m.%Y')
    creation_date.short_description = 'Створена'
    
    def publication_date(self, obj):
        return obj.published.strftime('%d.%m.%Y')
    publication_date.short_description = 'Опублікована'
    
    #~ def have_keywords(self, obj):
        #~ if (obj.keywords is not None) and (obj.keywords != ''):
            #~ return '<img alt="True" src="/static/admin/img/icon-yes.gif">'
        #~ else:
            #~ return '<img alt="True" src="/static/admin/img/icon-no.gif">'
    #~ have_keywords.allow_tags = True
    #~ have_keywords.short_description = 'Keywords'
    #~ 
    #~ def have_tags(self, obj):
        #~ if (obj.tag_list is not None) and (obj.tag_list != ''):
            #~ return '<img alt="True" src="/static/admin/img/icon-yes.gif">'
        #~ else:
            #~ return '<img alt="True" src="/static/admin/img/icon-no.gif">'
    #~ have_tags.allow_tags = True
    #~ have_tags.short_description = 'Tags'
    
    def uses_raw_html(self, obj):
        if obj.use_raw_html:
            return '<img alt="True" src="/static/admin/img/icon-yes.gif">'
        else:
            return '<img alt="True" src="/static/admin/img/icon-no.gif">'
    uses_raw_html.allow_tags = True
    uses_raw_html.short_description = 'Raw HTML'
