from django.contrib import admin


from entries.admin.chapters import ChaptersAdmin
from entries.admin.news import NewsAdmin
from entries.models import Chapter, NewsEntry


admin.site.register(Chapter, ChaptersAdmin)
admin.site.register(NewsEntry, NewsAdmin)
