# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Chapter'
        db.create_table('entries_chapter', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('short_title', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('full_title', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('show_title', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('slug', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128)),
            ('index', self.gf('django.db.models.fields.PositiveIntegerField')(default=10000)),
            ('ancestor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['entries.Chapter'], null=True, blank=True)),
            ('keywords', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('template', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('additional_news_kwargs', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('or_news_kwargs', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('exclipt_news_kwargs', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal('entries', ['Chapter'])

        # Adding M2M table for field placeholders on 'Chapter'
        m2m_table_name = db.shorten_name('entries_chapter_placeholders')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('chapter', models.ForeignKey(orm['entries.chapter'], null=False)),
            ('placeholder', models.ForeignKey(orm['placeholders.placeholder'], null=False))
        ))
        db.create_unique(m2m_table_name, ['chapter_id', 'placeholder_id'])

        # Adding model 'NewsEntry'
        db.create_table('entries_newsentry', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('chapter', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['entries.Chapter'])),
            ('show_title', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('short_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('full_title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('index', self.gf('django.db.models.fields.PositiveIntegerField')(default=10000)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('published', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('use_raw_html', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('full_body', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('short_body', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('full_body_html', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('short_body_html', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('tag_list', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('keywords', self.gf('django.db.models.fields.CharField')(max_length=4096, null=True, blank=True)),
            ('views_count', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal('entries', ['NewsEntry'])

        # Adding M2M table for field placeholders on 'NewsEntry'
        m2m_table_name = db.shorten_name('entries_newsentry_placeholders')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('newsentry', models.ForeignKey(orm['entries.newsentry'], null=False)),
            ('placeholder', models.ForeignKey(orm['placeholders.placeholder'], null=False))
        ))
        db.create_unique(m2m_table_name, ['newsentry_id', 'placeholder_id'])


    def backwards(self, orm):
        # Deleting model 'Chapter'
        db.delete_table('entries_chapter')

        # Removing M2M table for field placeholders on 'Chapter'
        db.delete_table(db.shorten_name('entries_chapter_placeholders'))

        # Deleting model 'NewsEntry'
        db.delete_table('entries_newsentry')

        # Removing M2M table for field placeholders on 'NewsEntry'
        db.delete_table(db.shorten_name('entries_newsentry_placeholders'))


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'entries.chapter': {
            'Meta': {'ordering': "['index', 'short_title']", 'object_name': 'Chapter'},
            'additional_news_kwargs': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'ancestor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entries.Chapter']", 'null': 'True', 'blank': 'True'}),
            'exclipt_news_kwargs': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'full_title': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.PositiveIntegerField', [], {'default': '10000'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'or_news_kwargs': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'placeholders': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['placeholders.Placeholder']", 'null': 'True', 'blank': 'True'}),
            'short_title': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'show_title': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'}),
            'template': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'entries.newsentry': {
            'Meta': {'ordering': "['index', '-published']", 'object_name': 'NewsEntry'},
            'chapter': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['entries.Chapter']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'full_body': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'full_body_html': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'full_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.PositiveIntegerField', [], {'default': '10000'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '4096', 'null': 'True', 'blank': 'True'}),
            'placeholders': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['placeholders.Placeholder']", 'null': 'True', 'blank': 'True'}),
            'published': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'short_body': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_body_html': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'short_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'show_title': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'tag_list': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'use_raw_html': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'views_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        'placeholders.placeholder': {
            'Meta': {'object_name': 'Placeholder'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['placeholders.PlaceholderGroup']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1000'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'template': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        'placeholders.placeholdergroup': {
            'Meta': {'object_name': 'PlaceholderGroup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1000'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        }
    }

    complete_apps = ['entries']