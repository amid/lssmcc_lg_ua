# -- coding: utf-8 --

from django.contrib import messages
from django.views.generic import DetailView


from entries.models import *


class ShowChapter(DetailView):
    
    http_method_names = ['get']
    model = Chapter
    
    def get_queryset(self):
        if self.request.user.has_perm('view_deleted_chapters'):
            return Chapter.objects.all()
        else:
            return Chapter.active_objects.all()
    
    def get_context_data(self, **kwargs):
        context = super(ShowChapter, self).get_context_data(**kwargs)
        obj = self.get_object()
        if not obj.is_active:
            messages.error(self.request, u'Этот раздел отмечен как "НЕ ДЕЙСТВУЮЩИЙ"')
        context['object_list'] = obj.news_entries()
        if not context['object_list']:
            messages.warning(self.request, u'В этом разделе нет опубликованых новостей.')
        context['current_chapter'] = obj
        context['current_menu_section'] = obj.slug
        #~ context['show_controls'] = 'show_controls' in map(lambda x: x['name'], obj.placeholders.all().values('name'))
        return context


class ShowNewsEntry(DetailView):
    
    http_method_names = ['get']
    model = NewsEntry
    
    def get_queryset(self):
        qs_kwargs = {
            'is_active': True,
            'is_published': True,
        }
        if self.request.user.has_perm('view_deleted_news'):
            del qs_kwargs['is_active']
        if self.request.user.has_perm('view_unpublished_news'):
            del qs_kwargs['is_published']
        return NewsEntry.objects.filter(**qs_kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(ShowNewsEntry, self).get_context_data(**kwargs)
        obj = self.get_object()
        context['current_chapter'] = obj.chapter
        context['current_menu_section'] = obj.chapter.slug
        #~ context['show_controls'] = 'show_controls' in map(lambda x: x['name'], obj.chapter.placeholders.all().values('name'))
        context['mode']= 'full'
        return context
        
