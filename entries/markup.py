# -- coding: utf-8 --

from entries.media import render_galery, render_inline

import re

def render_data(data, paragraph_tag='p'):
    
    # **text** = <strong>text</strong>
    # __text__ = <em>text</em>
    # * line - <li>line</li> (<ul>...</ul> or <ol>...</ol> needed)
    # [code:lang]...[/code] - <pre class="lang">...</pre>
    # [youtube]...[/youtube] - <div class="youtube"><iframe src="..." frameborder="0" height="480" width="640"></iframe</div>
    
    rendered_data = re.sub('^\*\s(?P<line>.*)', '<li>\g<line></li>', data, flags=re.MULTILINE)
    rendered_data = re.sub('\*\*(?P<text>.*)\*\*', '<strong>\g<text></strong>', rendered_data)
    rendered_data = re.sub('__(?P<text>.*)__', '<em>\g<text></em>', rendered_data)
    rendered_data = re.sub('\[code\:(?P<lang>.*)\](?P<src>.*)\[\/code\]', '<pre class="\g<lang>">\g<src></pre>', rendered_data, flags=re.DOTALL)
    rendered_data = re.sub('\[youtube\](?P<src>.*)\[\/youtube\]', '<div class="youtube"><iframe src="//www.youtube.com/embed/\g<src>" frameborder="0" height="480" width="640"></iframe></div>', rendered_data, flags=re.DOTALL)
    
    PARAGRAPH_TAGS = {
        'p': ('<p>', '</p>'),
        'div': ('<div>', '</div>')
    }
    
    # Ignoring tags <p>...</p>, <div ...>...</div>, <ol ...>...</ol>,  <ul ...>...</ul>, <table ...>...</table>,  <pre ...>...</pre> 
    TAGS = {
        'p': ('.*\<p\>.*', '.*\<\/p\>.*',),
        'div': ('.*\<div.*\>.*', '.*\<\/div\>.*',),
        'ol': ('.*\<ol.*\>.*', '.*\<\/ol\>.*',),
        'ul': ('.*\<ul.*\>.*', '.*\<\/ul\>.*',),
        'table': ('.*\<table.*\>.*', '.*\<\/table\>.*',),
        'pre': ('.*\<pre.*\>.*', '.*\<\/pre\>.*',),
    }
    
    rendered_data = rendered_data.split('\n')
    
    tag_found = None
    tag_found_counter = 0
    line_counter = 0
    
    for line in rendered_data:
        if line and line <> '':
            if not tag_found:
                for tag in TAGS.keys():
                    if re.match(TAGS[tag][0], line, flags=re.DOTALL):
                        tag_found = tag
                        tag_found_counter += 1
                if not tag_found:
                    rendered_data[line_counter] = PARAGRAPH_TAGS[paragraph_tag][0] + line + PARAGRAPH_TAGS[paragraph_tag][1]
            else:
                if re.match(TAGS[tag_found][0], line, flags=re.DOTALL):
                    tag_found_counter += 1
                if re.match(TAGS[tag_found][1], line, flags=re.DOTALL):
                    tag_found_counter -= 1
            if tag_found_counter == 0:
                tag_found = None
        line_counter += 1
    
    rendered_data = '\n'.join(rendered_data)
    
    # render galeries
    galery_re = '\[galery\spk="(?P<pk>.*)"\sname="(?P<name>.*)"\scols="(?P<cols>.*)"\sslideshow="(?P<slideshow>.*)"\]\[\/galery\]'
    rendered_data = re.sub(
        galery_re,
        lambda r: render_galery(
            pk = r.group(1),
            name = r.group(2),
            cols = r.group(3),
            slideshow = r.group(4)
        ),
        rendered_data,
        flags=re.MULTILINE
    )
    
    # render inlines from news media
    inline_re = '\[inline\spk="(?P<pk>.*)"\scaption="(?P<caption>.*)"\stype="(?P<type>.*)"\](?P<value>.*)\[\/inline\]'
    rendered_data = re.sub(
        inline_re,
        lambda r: render_inline(
            pk = r.group(1),
            caption = r.group(2),
            inline_type = r.group(3),
            value = r.group(4)
        ),
        rendered_data,
        flags=re.MULTILINE
    )
    
    # render external inlines
    inline_re = '\[inline\scaption="(?P<caption>.*)"\stype="(?P<type>.*)"\](?P<value>.*)\[\/inline\]'
    rendered_data = re.sub(
        inline_re,
        lambda r: render_inline(
            caption = r.group(1),
            inline_type = r.group(2),
            value = r.group(3)
        ),
        rendered_data,
        flags=re.MULTILINE
    )
    
    return rendered_data
