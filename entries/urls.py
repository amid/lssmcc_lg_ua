from django.conf.urls.defaults import *


from views import *


urlpatterns = patterns('',
    url(r'^chapter/(?P<slug>[^/]+)/$', ShowChapter.as_view(), name='show_chapter'),
    url(r'^entry/(?P<pk>[^/]+)/$', ShowNewsEntry.as_view(), name='show_news_entry'),
    #~ url(r'^tag/(?P<tag>[^/]+)/$', ListNewsByTag.as_view(), name='list_news_by_tag'),
    #~ url(r'^show/(?P<slug>[^/]+)/$', ShowNewsEntry.as_view(), name='show_news_entry'),
    #~ url(r'^create/$', CreateNewsEntry.as_view(), name='create_news_entry'),
    #~ url(r'^update/(?P<slug>[^/]+)/$', UpdateNewsEntry.as_view(), name='update_news_entry'),
    #~ url(r'^search/$', search, name='search'),
)
