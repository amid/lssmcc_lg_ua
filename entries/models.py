# -- coding: utf-8 --

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver


from entries.media import get_general_path, get_item_dir_path, get_item_dir_url, render_galery_markup, render_inline_markup
from markup import render_data
from placeholders.models import *


from pytils.translit import slugify
from tagging.models import Tag
import ast, datetime, os, shutil


MARKDOWN_PARAGRAPH_ELEMENT = getattr(settings, 'MARKDOWN_PARAGRAPH_ELEMENT', 'p')


class ActiveManager(models.Manager):
    def get_query_set(self):
        return super(ActiveManager, self).get_query_set().filter(is_active=True)


class Chapter(models.Model):
    
    is_active = models.BooleanField(default=True, verbose_name=u'діючий')
    short_title = models.CharField(max_length=128, verbose_name=u'скорочений заголовок')
    full_title = models.CharField(max_length=128, blank=True, null=True, verbose_name=u'заголовок')
    show_title = models.BooleanField(default=True, verbose_name=u'відображати заголовок')
    slug = models.CharField(max_length=128, unique=True, help_text='якщо залишити пустим - буде автоматично зроблено із скороченного заголовку')
    index = models.PositiveIntegerField(default=10000, verbose_name=u'індекс')
    ancestor = models.ForeignKey('self', blank=True, null=True, verbose_name=u'головний розділ')
    keywords = models.CharField(max_length=255, blank=True, null=True, verbose_name=u'keywords', help_text=u'на сторінці зі списком новин розділу')
    template = models.CharField(max_length=255, blank=True, null=True, verbose_name=u'шаблон', help_text='використовується для розділів з фіксованими шаблонами')
    url = models.CharField(max_length=255, blank=True, null=True, verbose_name=u'посилання', help_text='використовується для дінамічних розділів')
    placeholders = models.ManyToManyField(Placeholder, blank=True, null=True, verbose_name=u'placeholders')
    additional_news_kwargs = models.CharField(max_length=255, null=True, blank=True, verbose_name='дополнительные параметры выборки новостей', help_text='словарь python с django queryset kwargs')
    or_news_kwargs = models.CharField(max_length=255, null=True, blank=True, verbose_name='альтернативные параметры выборки новостей', help_text='словарь python с django queryset kwargs')
    exclipt_news_kwargs = models.CharField(max_length=255, null=True, blank=True, verbose_name='явные параметры выборки новостей', help_text='словарь python с django queryset kwargs')
    
    objects = models.Manager()
    active_objects = ActiveManager()
    
    class Meta:
        ordering = ['index', 'short_title']
        verbose_name = u'розділ'
        verbose_name_plural = u'розділи'
    
    def save(self, **kwargs):
        if not self.slug:
            self.slug = slugify(self.short_title)
        super(Chapter, self).save(**kwargs)
    
    def __unicode__(self):
        return self.short_title
    
    def get_full_title(self):
        return getattr(self, 'short_title', 'full_title')
    
    def child_chapters(self):
        return Chapter.active_objects.filter(ancestor=self)
    
    def news_entries(self):
        if self.exclipt_news_kwargs:
            return NewsEntry.published_news.filter(**ast.literal_eval(self.exclipt_news_kwargs))
        else:
            kwargs = {'chapter': self}
            if self.additional_news_kwargs:
                kwargs.update(**ast.literal_eval(self.additional_news_kwargs))
            if self.or_news_kwargs:
                return NewsEntry.published_news.filter(models.Q(**kwargs) | models.Q(**ast.literal_eval(self.or_news_kwargs)))
            else:
                return NewsEntry.published_news.filter(**kwargs)
    
    def get_absolute_url(self):
        if self.url:
            return self.url
        else:
            return reverse('entries:show_chapter', kwargs={'slug': self.slug})
    
    def news_count(self):
        news = self.news_entries()
        if news:
            return len(self.news_entries())
        else:
            return 0

class UnpublishedNewsManager(models.Manager):
    def get_query_set(self):
        return super(UnpublishedNewsManager, self).get_query_set().filter(is_active=True, is_published=False)


class PublishedNewsManager(models.Manager):
    def get_query_set(self):
        return super(PublishedNewsManager, self).get_query_set().filter(is_active=True, is_published=True)


class HomepageNewsManager(models.Manager):
    def get_query_set(self):
        return super(HomepageNewsManager, self).get_query_set().filter(is_active=True, is_published=True, show_on_homepage=True)


class NewsEntry(models.Model):

    is_active = models.BooleanField(default=True, verbose_name=u'діюча')
    chapter = models.ForeignKey('Chapter', verbose_name=u'розділ')
    show_title = models.BooleanField(default=True, verbose_name=u'відображати заголовок')
    short_title = models.CharField(max_length=255, verbose_name=u'скорочений заголовок')
    full_title= models.CharField(max_length=255, blank=True, null=True, verbose_name=u'повний заголовок')
    index = models.PositiveIntegerField(default=10000, verbose_name=u'індекс')
    created = models.DateTimeField(auto_now=True, verbose_name='дата створення')
    published = models.DateTimeField(blank=True, null=True, verbose_name='дата публікації')
    is_published = models.BooleanField(default=False, verbose_name=u'опублікована')
    use_raw_html = models.BooleanField(default=False, verbose_name=u'відображати тільки у форматі HTML', help_text=u'поля повный текст та текст для відображення ігноруються')
    full_body = models.TextField(blank=True, null=True, verbose_name='повный текст')
    short_body = models.TextField(blank=True, null=True, verbose_name='текст для відображення')
    full_body_html = models.TextField(blank=True, null=True, verbose_name='повный текст (HTML)')
    short_body_html = models.TextField(blank=True, null=True, verbose_name='текст для відображення (HTML)')
    tag_list = models.CharField(max_length=255, blank=True, null=True, verbose_name=u'теги')
    keywords = models.CharField(max_length=4096, blank=True, null=True, verbose_name=u'keywords', help_text=u'на сторінці із відображенням новини')
    views_count = models.PositiveIntegerField(default=0, editable=False, verbose_name=u'перегляди')
    placeholders = models.ManyToManyField(Placeholder, blank=True, null=True, verbose_name=u'placeholders')
    
    objects = models.Manager()
    active_objects = ActiveManager()
    unpublished_news = UnpublishedNewsManager()
    published_news = PublishedNewsManager()
    homepage_news = HomepageNewsManager()

    class Meta:
        
        ordering = ['index', '-published']
        verbose_name = u'новина'
        verbose_name_plural = u'новини'
    
    def _get_tags(self):
        return Tag.objects.get_for_object(self)

    def _set_tags(self, tag_list):
        Tag.objects.update_tags(self, tag_list)

    tags = property(_get_tags, _set_tags)
    
    def save(self, **kwargs):
        if self.is_published and self.published is None:
            self.published = datetime.datetime.now()
        if not self.is_published and self.published is not None:
            self.published = None
        if not self.use_raw_html:
            self.full_body_html = render_data(self.full_body, paragraph_tag=MARKDOWN_PARAGRAPH_ELEMENT)
            self.short_body_html = render_data(self.short_body, paragraph_tag=MARKDOWN_PARAGRAPH_ELEMENT)
        super(NewsEntry, self).save(**kwargs)
        self.tags = self.tag_list

    def __unicode__(self):
        return self.short_title

    def get_title(self):
        if self.short_title:
            return self.short_title
        else:
            return self.full_title
    
    def get_body_html(self):
        if self.short_body_html:
            return self.short_body_html
        else:
            return self.full_body_html
    
    def show_controls(self):
        return self.chapter.placeholders.filter(name='show_controls').exists() and not self.placeholders.filter(name='do_not_show_controls_in_news').exists()
    
    def always_show_attachments(self):
        return self.placeholders.filter(name='always_show_attachments').exists()
    
    def get_absolute_url(self):
        return reverse('entries:show_news_entry', kwargs={'pk': self.pk})
       
    def collect_galeries(self):
        result = []
        list_dir = os.listdir(get_item_dir_path(self.pk, 'galery'))
        list_dir.sort()
        for item in list_dir:
            if os.path.isdir(os.path.join(get_item_dir_path(self.pk, 'galery'), item)):
                result.append(
                    {
                        'name': item,
                        'markup': render_galery_markup(self.pk, item)
                    }
                )
        return result
    
    def collect_inlines(self):
        result = []
        list_dir = os.listdir(get_item_dir_path(self.pk, 'inline'))
        list_dir.sort()
        for item in list_dir:
            if os.path.isfile(os.path.join(get_item_dir_path(self.pk, 'inline'), item)):
                result.append(
                    {
                        'name': item,
                        'markup': render_inline_markup(self.pk, item)
                    }
                )
        return result
    
    def collect_attachments(self):
        result = []
        list_dir = os.listdir(get_item_dir_path(self.pk, 'attachment'))
        list_dir.sort()
        for item in list_dir:
            if os.path.isfile(os.path.join(get_item_dir_path(self.pk, 'attachment'), item)):
                result.append(
                    {
                        'name': item,
                        'href': '/'.join(['', settings.MEDIA_URL.replace('/', ''), 'news', str(self.pk), 'attachment', item])
                    }
                )
        return result
    
    def get_media_items(self):
        result = {}
        result['galeries'] = self.collect_galeries()
        result['inlines'] = self.collect_inlines()
        return result

@receiver(post_save, sender=NewsEntry)
def create_dirs(sender, **kwargs):
    if kwargs['created']:
        try:
            os.makedirs(get_item_dir_path(kwargs['instance'].id, 'attachment'), mode=0755)
            os.makedirs(get_item_dir_path(kwargs['instance'].id, 'galery'), mode=0755)
            os.makedirs(get_item_dir_path(kwargs['instance'].id, 'inline'), mode=0755)
        except:
            pass


@receiver(post_delete, sender=NewsEntry)
def rm_dirs(sender, **kwargs):
    try:
        shutil.rmtree(get_general_path(kwargs['instance'].id))
    except:
        pass
