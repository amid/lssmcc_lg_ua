from django.conf import settings


from sorl.thumbnail import get_thumbnail
import os


def get_general_path(pk):
    return os.path.join(settings.MEDIA_ROOT, 'news', str(pk))


def get_general_url(pk):
    return os.path.join(settings.MEDIA_URL, 'news', str(pk))


def get_item_dir_path(pk, dirname):
    return os.path.join(get_general_path(pk), dirname)


def get_item_dir_url(pk, dirname):
    return os.path.join(get_general_url(pk), dirname)


def render_galery(pk=None, name='', cols='4', slideshow='0'):
    try:
        cols = int(cols)
        result = '<div class="row-fluid galery">'
        galery_path = os.path.join(get_general_path(pk), 'galery', name)
        counter = cols
        list_dir = os.listdir(galery_path)
        list_dir.sort()
        for item in list_dir:
            if counter <= 0:
                result += '</div><div class="row-fluid galery">'
                counter = cols
            item_path = os.path.join(galery_path, item)
            if os.path.isfile(item_path):
                thumb = get_thumbnail(
                    os.path.join(get_item_dir_path(pk, 'galery'), name, item),
                    '%dx%d' % (int(870/cols), int((870/cols)*3/4)),
                    crop='center',
                    quality=85
                )
                result +='<div class="span%d"><a class="cboxElement" rel="gallery_slideshow" href="%s"><img class="entry-image" src="%s"></a></div>' % (
                    int(12/cols),
                    '/'.join(['', settings.MEDIA_URL.replace('/', ''), 'news', str(pk), 'galery', name, item]),
                    thumb.url
                )
                counter -= 1
        return result+'</div>'
    except:
        return ''


def render_galery_markup(pk, name):
    return '[galery pk="%d" name="%s" cols="4" slideshow="1"][/galery]' % (pk, name)


def render_inline(pk=None, value='', caption='', inline_type=''):
    if pk is None:
        if inline_type=='ext_link':
            return '<a href="%s">%s</a>' % (value, caption)
        if inline_type=='ext_img':
            return '<div><img class="entry-image" src="%s" alt="%s" /></div>' % (value, caption)
    else:
        if inline_type=='link':
            return '<a href="%s">%s</a>' % (os.path.join(get_item_dir_url(pk, 'inline'), value), caption)
        if inline_type=='img':
            return '<div><img class="entry-image" src="%s" alt="%s" /></div>' % (os.path.join(get_item_dir_url(pk, 'inline'), value), caption)
    return ''


def render_inline_markup(pk, name):
    return '[inline pk="%d" caption="%s" type="link"]%s[/inline]' % (pk, name, name)
