# -- coding: utf-8 --

from django.core.management import setup_environ
from lssmcc_lg_ua import settings


setup_environ(settings)


from placeholders.models import *
from entries.models import *


from django.contrib.contenttypes.models import ContentType


# Placeholder groups

placeholder_group_sidebar_menu = PlaceholderGroup.objects.get_or_create(
    name = u'sidebar_menu',
    title = u'Меню',
)[0]

placeholder_group_chapters = PlaceholderGroup.objects.get_or_create(
    name = u'chapters',
    title = u'Разделы',
)[0]

placeholder_group_news = PlaceholderGroup.objects.get_or_create(
    name = u'news',
    title = u'Новости',
)[0]

# Placeholder: Show controls (in chapters)

placeholder_show_controls = Placeholder.objects.get_or_create(
    name = u'show_controls',
    title = u'Отображать даты, ссылки, теги и т.д.',
    index = 10,
    group = placeholder_group_chapters,
    content_type = ContentType.objects.get(app_label='entries', model='chapter')
)[0]

# Placeholder: Do not show controls in news

placeholder_show_controls = Placeholder.objects.get_or_create(
    name = u'do_not_show_controls_in_news',
    title = u'Не показывать даты, ссылки, теги и т.д. (для новости)',
    index = 10,
    group = placeholder_group_news,
    content_type = ContentType.objects.get(app_label='entries', model='newsentry')
)[0]

# Placeholder: Show on homepage

placeholder_show_on_homepage = Placeholder.objects.get_or_create(
    name = u'show_on_homepage',
    title = u'Отображать на главной странице',
    index = 10,
    group = placeholder_group_news,
    content_type = ContentType.objects.get(app_label='entries', model='newsentry')
)[0]

# Placeholder: Main menu

placeholder_main_menu = Placeholder.objects.get_or_create(
    name = u'main_menu',
    title = u'Главное меню',
    index = 20,
    content_type = ContentType.objects.get(app_label='entries', model='chapter')
)[0]

# Placeholders: Sidebar menus

placeholder_sidebar_navigation_menu = Placeholder.objects.get_or_create(
    name = u'sidebar_navigation_menu',
    title = u'Навигация по сайту',
    index = 30,
    group = placeholder_group_sidebar_menu,
    content_type = ContentType.objects.get(app_label='entries', model='chapter')
)[0]

placeholder_sidebar_news_and_publications_menu = Placeholder.objects.get_or_create(
    name = u'sidebar_news_and_publications_menu',
    title = u'Новости и публикации',
    index = 40,
    group = placeholder_group_sidebar_menu,
    content_type = ContentType.objects.get(app_label='entries', model='chapter')
)[0]

placeholder_sidebar_projects_menu = Placeholder.objects.get_or_create(
    name = u'sidebar_projects_menu',
    title = u'Проекты',
    index = 50,
    group = placeholder_group_sidebar_menu,
    content_type = ContentType.objects.get(app_label='entries', model='chapter')
)[0]

placeholder_sidebar_information_menu = Placeholder.objects.get_or_create(
    name = u'sidebar_information_menu',
    title = u'Информация о предприятии',
    index = 60,
    group = placeholder_group_sidebar_menu,
    content_type = ContentType.objects.get(app_label='entries', model='chapter')
)[0]

# Placeholders: Sidebar filters

placeholder_show_in_anouncements = Placeholder.objects.get_or_create(
    name = u'show_in_anouncements',
    title = u'Отображать в анонсах',
    index = 30,
    group = placeholder_group_news,
    content_type = ContentType.objects.get(app_label='entries', model='newsentry')
)[0]

placeholder_show_in_news = Placeholder.objects.get_or_create(
    name = u'show_in_news',
    title = u'Отображать в новостях',
    index = 20,
    group = placeholder_group_news,
    content_type = ContentType.objects.get(app_label='entries', model='newsentry')
)[0]

placeholder_show_in_publications = Placeholder.objects.get_or_create(
    name = u'show_in_publications',
    title = u'Отображать в публикациях',
    group = placeholder_group_news,
    index = 40,
    content_type = ContentType.objects.get(app_label='entries', model='newsentry')
)[0]

# Filter groups

filter_group_sidebar_chapters = FilterGroup.objects.get_or_create(
    name = u'sidebar_chapters',
    title = u'Разделы в меню',
)[0]

filter_group_sidebar_filters = FilterGroup.objects.get_or_create(
    name = u'sidebar_filters',
    title = u'Фильтры в меню',
)[0]

# Filter: Show controls

#~ filter_show_controls = Filter.objects.get_or_create(
    #~ name = u'show_controls',
    #~ title = u'Відображати дати, посилання, теги, тощо',
    #~ content_type = ContentType.objects.get(app_label='entries', model='chapter'),
    #~ element_kwargs = '{"placeholders__name": "show_controls", "is_active": True}',
    #~ show_if_empty = False,
#~ )[0]

# Filter: Show on homepage

filter_show_on_homepage = Filter.objects.get_or_create(
    name = u'show_on_homepage',
    title = u'Отображать на главной странице',
    content_type = ContentType.objects.get(app_label='entries', model='newsentry'),
    element_kwargs = '{"placeholders__name": "show_on_homepage", "is_active": True, "is_published": True}',
    element_template = 'homepage/homepage_news_entry.html',
    show_if_empty = False,
)[0]

# Filter: Main menu

filter_main_menu = Filter.objects.get_or_create(
    name = u'main_menu',
    title = u'Главное меню',
    content_type = ContentType.objects.get(app_label='entries', model='chapter'),
    element_kwargs = '{"placeholders__name": "main_menu", "is_active": True}',
    element_template = 'menu/main_menu.html',
    show_if_empty = False,
)[0]

# Filters: Sidebar menus

filter_sidebar_navigation_menu = Filter.objects.get_or_create(
    name = u'sidebar_navigation_menu',
    title = u'Навигация по сайту',
    group = filter_group_sidebar_chapters,
    content_type = ContentType.objects.get(app_label='entries', model='chapter'),
    element_kwargs = '{"placeholders__name": "sidebar_navigation_menu", "is_active": True}',
    element_template = 'menu/sidebar_submenu.html',
    show_if_empty = False,
)[0]

filter_sidebar_news_and_publications_menu = Filter.objects.get_or_create(
    name = u'sidebar_news_and_publications_menu',
    title = u'Новости и публикации',
    group = filter_group_sidebar_chapters,
    content_type = ContentType.objects.get(app_label='entries', model='chapter'),
    element_kwargs = '{"placeholders__name": "sidebar_news_and_publications_menu", "is_active": True}',
    element_template = 'menu/sidebar_submenu.html',
    show_if_empty = False,
)[0]

filter_sidebar_projects_menu = Filter.objects.get_or_create(
    name = u'sidebar_projects_menu',
    title = u'Проекты',
    group = filter_group_sidebar_chapters,
    content_type = ContentType.objects.get(app_label='entries', model='chapter'),
    element_kwargs = '{"placeholders__name": "sidebar_projects_menu", "is_active": True}',
    element_template = 'menu/sidebar_submenu.html',
    show_if_empty = False,
)[0]

filter_sidebar_information_menu = Filter.objects.get_or_create(
    name = u'sidebar_information_menu',
    title = u'Информация о предприятии',
    group = filter_group_sidebar_chapters,
    content_type = ContentType.objects.get(app_label='entries', model='chapter'),
    element_kwargs = '{"placeholders__name": "sidebar_information_menu", "is_active": True}',
    element_template = 'menu/sidebar_submenu.html',
    show_if_empty = False,
)[0]

# Filters: Sidebar filters

filter_latest_anouncements = Filter.objects.get_or_create(
    name = u'latest_anouncements',
    title = u'Последние объявления',
    group = filter_group_sidebar_filters,
    content_type = ContentType.objects.get(app_label='entries', model='newsentry'),
    element_kwargs = '{"placeholders__name": "show_in_anouncements", "is_active": True, "is_published": True}',
    element_template = 'menu/sidebar_submenu.html',
    element_ordering = '-published',
    element_limit = 5,
    show_if_empty = True,
)[0]

filter_latest_news = Filter.objects.get_or_create(
    name = u'latest_news',
    title = u'Последние новости',
    group = filter_group_sidebar_filters,
    content_type = ContentType.objects.get(app_label='entries', model='newsentry'),
    element_kwargs = '{"placeholders__name": "show_in_news", "is_active": True, "is_published": True}',
    element_template = 'menu/sidebar_submenu.html',
    element_ordering = '-published',
    element_limit = 5,
    show_if_empty = True,
)[0]

filter_latest_publications = Filter.objects.get_or_create(
    name = u'latest_publications',
    title = u'Последние публикации',
    group = filter_group_sidebar_filters,
    content_type = ContentType.objects.get(app_label='entries', model='newsentry'),
    element_kwargs = '{"placeholders__name": "show_in_publications", "is_active": True, "is_published": True}',
    element_template = 'menu/sidebar_submenu.html',
    element_ordering = '-published',
    element_limit = 5,
    show_if_empty = True,
)[0]

# Chapters: Main menu

chapter_metro, created = Chapter.objects.get_or_create(
    short_title = u'Метрология',
    index = 10,
    slug = 'metrologiya',
)
if created:
    chapter_metro.placeholders.add(placeholder_main_menu)

chapter_sert, created = Chapter.objects.get_or_create(
    short_title = u'Сертификация',
    index = 20,
    slug = 'sertifikatciya',
)
if created:
    chapter_sert.placeholders.add(placeholder_main_menu)

chapter_standart, created = Chapter.objects.get_or_create(
    short_title = u'Стандартизация',
    index = 30,
    slug = 'standartizaciya',
)
if created:
    chapter_standart.placeholders.add(placeholder_main_menu)

chapter_lab, created = Chapter.objects.get_or_create(
    short_title = u'Испытания',
    index = 40,
    slug = 'vyprobuvannya',
)
if created:
    chapter_lab.placeholders.add(placeholder_main_menu)

chapter_konkurs, created = Chapter.objects.get_or_create(
    short_title = u'Конкурсы',
    index = 50,
    slug = 'konkursy',
)
if created:
    chapter_konkurs.placeholders.add(placeholder_main_menu)

chapter_contacts, created = Chapter.objects.get_or_create(
    short_title = u'Контакты',
    index = 13000,
    slug = 'kontakty',
    template = 'contacts.html',
)
if created:
    chapter_contacts.placeholders.add(placeholder_main_menu)
    chapter_contacts.placeholders.add(placeholder_sidebar_information_menu)

# Chapters: Main menu subchapters

chapter_mehan, created = Chapter.objects.get_or_create(
    short_title = u'Механические измерения',
    index = 110,
    slug = 'mehan',
    ancestor = chapter_metro,
)

chapter_radio, created = Chapter.objects.get_or_create(
    short_title = u'Радиотехнические измерения',
    index = 120,
    slug = 'radio',
    ancestor = chapter_metro,
)

chapter_teplo, created = Chapter.objects.get_or_create(
    short_title = u'Теплотехнические измерения',
    index = 130,
    slug = 'teplo',
    ancestor = chapter_metro,
)

chapter_elect, created = Chapter.objects.get_or_create(
    short_title = u'Электрические измерения',
    index = 140,
    slug = 'elect',
    ancestor = chapter_metro,
)

chapter_fh, created = Chapter.objects.get_or_create(
    short_title = u'Физико-химические измерения',
    index = 150,
    slug = 'fiz-him',
    ancestor = chapter_metro,
)

chapter_atest, created = Chapter.objects.get_or_create(
    short_title = u'Атестация лабораторий',
    index = 160,
    slug = 'atest',
    ancestor = chapter_metro,
)

chapter_dmn, created = Chapter.objects.get_or_create(
    short_title = u'Государственный метрологический надзор', # R.I.P.
    index = 170,
    slug = 'dmn',
    ancestor = chapter_metro,
)

chapter_promsert, created = Chapter.objects.get_or_create(
    short_title = u'Промышленная продукция',
    index = 210,
    slug = 'promsert',
    ancestor = chapter_sert,
)

chapter_sertpi, created = Chapter.objects.get_or_create(
    short_title = u'Пищевая продукция',
    index = 220,
    slug = 'pisert',
    ancestor = chapter_sert,
)

chapter_sq, created = Chapter.objects.get_or_create(
    short_title = u'Системы управления',
    index = 220,
    slug = 'suya',
    ancestor = chapter_sert,
)

chapter_inf, created = Chapter.objects.get_or_create(
    short_title = u'Информационное обслуживание',
    index = 310,
    slug = 'inform',
    ancestor = chapter_standart,
)

chapter_redyak, created = Chapter.objects.get_or_create(
    short_title = u'Содействие предпринимательству',
    index = 320,
    slug = 'spriyannya-pidpryemnyctvu',
    ancestor = chapter_standart,
)

chapter_top100, created = Chapter.objects.get_or_create(
    short_title = u'100 лучших товаров Украины',
    index = 510,
    slug = 'top100',
    ancestor = chapter_konkurs,
)

chapter_risunok, created = Chapter.objects.get_or_create(
    short_title = u'Всеукраинская выставка-конкурс детского рисунка',
    index = 520,
    slug = 'malunok',
    ancestor = chapter_konkurs,
)

chapter_snd, created = Chapter.objects.get_or_create(
    short_title = u'На соискание премии СНГ',
    index = 530,
    slug = 'snd',
    ancestor = chapter_konkurs,
)

# Chapters: Information dep. subchapters ### God, bless our information department!

subchapter_inf1, created = Chapter.objects.get_or_create(
    short_title = u'Проверка ТУ',
    index = 910,
    slug = 'tu',
    ancestor = chapter_inf,
)

subchapter_inf2, created = Chapter.objects.get_or_create(
    short_title = u'Административные услуги',
    index = 920,
    slug = 'admin-poslugy-inf',
    ancestor = chapter_inf,
)

subchapter_inf3, created = Chapter.objects.get_or_create(
    short_title = u'Реестр технических регламентов',
    index = 930,
    slug = 'reestr-tehnichnyh-reglamentiv',
    ancestor = chapter_inf,
)

subchapter_inf4, created = Chapter.objects.get_or_create(
    short_title = u'Технические регламенты Украины',
    index = 940,
    slug = 'tehnichni-reglamenty-ukrainy',
    ancestor = chapter_inf,
)

subchapter_inf5, created = Chapter.objects.get_or_create(
    short_title = u'Приказы о действии НД',
    index = 950,
    slug = 'nakazy-schodo-chynnosti-nd',
    ancestor = chapter_inf,
)

subchapter_inf6, created = Chapter.objects.get_or_create(
    short_title = u'Новые поступления НД',
    index = 960,
    slug = 'novi-nadhodzhennya-nd',
    ancestor = chapter_inf,
)

subchapter_inf7, created = Chapter.objects.get_or_create(
    short_title = u'Официальные копии НД',
    index = 970,
    slug = 'oficijni-kopii-nd',
    ancestor = chapter_inf,
)

subchapter_inf8, created = Chapter.objects.get_or_create(
    short_title = u'Актуализация НД',
    index = 980,
    slug = 'aktualizaciya-nd',
    ancestor = chapter_inf,
)

subchapter_inf9, created = Chapter.objects.get_or_create(
    short_title = u'Информационный бюллетень по Международной стандартизации',
    index = 990,
    slug = 'informatsjnij-byuleten-iz-mizhnarodnoi-standartyzacii',
    ancestor = chapter_inf,
)

# Chapters: Sidebar chspters

chapter_na_golovnu, created = Chapter.objects.get_or_create(
    short_title = u'На главную',
    index = 5,
    slug = 'na-golovny',
    url = u'/',
)
if created:
    chapter_na_golovnu.placeholders.add(placeholder_sidebar_navigation_menu)
    chapter_na_golovnu.placeholders.add(placeholder_main_menu)

chapter_search, created = Chapter.objects.get_or_create(
    short_title = u'Поиск',
    index = 2000,
    slug = 'poshuk',
    url = u'/entries/search/',
)
if created:
    chapter_search.placeholders.add(placeholder_sidebar_navigation_menu)

chapter_sitemap, created = Chapter.objects.get_or_create(
    short_title = u'Карта сайта',
    index = 3000,
    slug = 'map',
    template = u'sitemap.html',
)
if created:
    chapter_sitemap.placeholders.add(placeholder_sidebar_navigation_menu)

chapter_feedback, created = Chapter.objects.get_or_create(
    short_title = u"Обратная связь",
    index = 4000,
    slug = 'feedback',
    url = u'/feedback/',
)
if created:
    chapter_feedback.placeholders.add(placeholder_sidebar_navigation_menu)

chapter_anouncemenets, created = Chapter.objects.get_or_create(
    short_title = u'Объявления',
    index = 5000,
    slug = 'ogoloshennya',
    exclipt_news_kwargs = '{"chapter__slug": "novini", "placeholders__name": "show_in_anouncements"}'
)
if created:
    chapter_anouncemenets.placeholders.add(placeholder_sidebar_news_and_publications_menu)

chapter_news, created = Chapter.objects.get_or_create(
    short_title = u'Новости',
    index = 6000,
    slug = 'novyny',
    or_news_kwargs= '{"placeholders__name": "show_in_news"}',
)
if created:
    chapter_news.placeholders.add(placeholder_sidebar_news_and_publications_menu)

chapter_publications, created = Chapter.objects.get_or_create(
    short_title = u'Наши публикации',
    index = 7000,
    slug = 'nashi-publikacii',
    exclipt_news_kwargs = '{"chapter__slug": "novini", "placeholders__name": "show_in_publications"}'
)
if created:
    chapter_publications.placeholders.add(placeholder_sidebar_news_and_publications_menu)

chapter_koshik, created = Chapter.objects.get_or_create(
    short_title = u'Телепроект «Что в корзине»',
    index = 8000,
    slug = 'teleproekt-scho-u-koshyku',
)
if created:
    chapter_koshik.placeholders.add(placeholder_sidebar_projects_menu)

chapter_forum, created = Chapter.objects.get_or_create(
    short_title = u'Форум потребителей',
    index = 9000,
    slug = 'forum',
    url = 'http://forum.lssmcc.lg.ua/',
)
if created:
    chapter_forum.placeholders.add(placeholder_sidebar_projects_menu)

chapter_sert_enterprises, created = Chapter.objects.get_or_create(
    short_title = u'Cертифицированные предприятия',
    index = 10000,
    slug = 'certyfkovani-pidpryemstva',
    template = 'sert_enterprises.html',
)
if created:
    chapter_sert_enterprises.placeholders.add(placeholder_sidebar_projects_menu)

chapter_about, created = Chapter.objects.get_or_create(
    short_title = u'О предприятии',
    index = 11000,
    slug = 'pro-pidpryemstvo',
)
if created:
    chapter_about.placeholders.add(placeholder_sidebar_information_menu)

chapter_bosses, created = Chapter.objects.get_or_create(
    short_title = u'Руководство',
    index = 12000,
    slug = 'kerivnyctvo',
    template = 'bosses.html',
)
if created:
    chapter_bosses.placeholders.add(placeholder_sidebar_information_menu)

chapter_structure, created = Chapter.objects.get_or_create(
    short_title = u'Структура',
    index = 14000,
    slug = 'structure',
)
if created:
    chapter_structure.placeholders.add(placeholder_sidebar_information_menu)

chapter_science, created = Chapter.objects.get_or_create(
    short_title = u'Научно-техническая деятельность',
    index = 15000,
    slug = 'ntr',
)
if created:
    chapter_science.placeholders.add(placeholder_sidebar_information_menu)

chapter_admin_poslugy, created = Chapter.objects.get_or_create(
    short_title = u'Административные услуги',
    index = 16000,
    slug = 'adminstratyvni-poslugy',
)
if created:
    chapter_admin_poslugy.placeholders.add(placeholder_sidebar_information_menu)

chapter_fucking_tenders, created = Chapter.objects.get_or_create(
    short_title = u'Государственные закупки',
    index = 17000,
    slug = 'derzhavni-zakupvli',
)
if created:
    chapter_fucking_tenders.placeholders.add(placeholder_sidebar_information_menu)

chapter_uzps, created = Chapter.objects.get_or_create(
    short_title = u'Защита потребителей',
    index = 19000,
    slug = 'zahyst-spozhyvachiv',
)
if created:
    chapter_uzps.placeholders.add(placeholder_sidebar_information_menu)

chapter_public_info, created = Chapter.objects.get_or_create(
    short_title = u'Доступ к публичной информации',
    index = 20000,
    slug = 'public-info',
)
if created:
    chapter_public_info.placeholders.add(placeholder_sidebar_information_menu)

chapter_documents, created = Chapter.objects.get_or_create(
    short_title = u'Образцы документов',
    index = 21000,
    slug = '',
)
if created:
    chapter_documents.placeholders.add(placeholder_sidebar_information_menu)

#~ entry = open('./data/about/about.entry', 'r')
#~ 
#~ news_about = NewsEntry.objects.create(
    #~ chapter = chapter_about,
    #~ index = 10000,
    #~ short_title = u'### Інформація про підприємство',
    #~ show_title = False,
    #~ is_published = True,
    #~ full_body = entry.read()
#~ )
#~ 
#~ entry.close()
